    var userName=document.getElementById("name");
    var namevalidcheck=document.getElementById("nameCheck");
    var lastname=document.getElementById("fathername");
    var lnamevalidcheck=document.getElementById("lNameCheck");
    var useremail=document.getElementById("email");
    var useremailcheck=document.getElementById("emailvalidcheck");
    var dateOfBirth=document.getElementById("DOB");
    var dateOfBirthcheck=document.getElementById("dobvalidcheck");
    var mobileNumber=document.getElementById("mobilenumber");
    var mobileNumberValidCheck=document.getElementById("mobilevalidcheck");
    var addressLine1=document.getElementById("addressLine1");
    var addressLine2=document.getElementById("addressLine2");
    var addressvalidcheck=document.getElementById("addressvalidcheck");
    var city=document.getElementById("city");
    var cityvalidcheck=document.getElementById("cityvalidcheck");
    var pincode=document.getElementById("pincode");
    var pincodevalidcheck=document.getElementById("pincodevalidcheck");
    var checkbox=document.getElementById("checkbox");

    var nameRegx=/^[a-zA-Z]{3,20}$/i;
    var lastRegx=/^[a-zA-Z]{3,20}$/i;
    var emailRegx=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;;
    var dateRegx=/[0-3][0-9][/][0-1][0-9][/][0-9]{4}$/;
    var mobileRegx=/^[6-9][0-9]{9}$/;
    var addressRegx=/[A-Za-z0-9'\.\-\s\,]/;
    var cityRegx=/^[a-zA-Z]{3,20}$/i;
    var zipRegx=/^[0-9]{6}$/i;
function validation(){

    // Name validation
    
 
  
    if(userName.value.trim()==""){
        namevalidcheck.innerHTML="Username is required";
        userName.style.border="1px solid red";
        namevalidcheck.style.color="red";
        namevalidcheck.style.visibility="visible";
        // return false;
    }
    else if(userName.value.length<3){
        namevalidcheck.innerHTML="Name should be minimum 3 character";
        userName.style.border="1px solid red";
        namevalidcheck.style.color="red";
        namevalidcheck.style.visibility="visible";
        // return false;
    }
    else if(!nameRegx.test(userName.value)){
        namevalidcheck.innerHTML="Special character and numbers are not allowed";
        userName.style.border="1px solid red";
        namevalidcheck.style.color="red";
        namevalidcheck.style.visibility="visible";
        // return false;
    }
      
    else{
        namevalidcheck.innerHTML="vaild";
        namevalidcheck.style.color="green";
        namevalidcheck.style.visibility="visible";  
    }
    

    //Last name

    if(lastname.value.trim()==""){
        lnamevalidcheck.innerHTML="Lastname is required";
        lastname.style.border="1px solid red";
        lnamevalidcheck.style.color="red";
        lnamevalidcheck.style.visibility="visible";
        // return false;
    }
    else if(lastname.value.length<3){
        lnamevalidcheck.innerHTML="Name should be minimum 3 character";
        lastname.style.border="1px solid red";
        lnamevalidcheck.style.color="red";
        lnamevalidcheck.style.visibility="visible";
        // return false;
    }
    else if(!lastRegx.test(lastname.value)){
        lnamevalidcheck.innerHTML="Special character and numbers are not allowed";
        lastname.style.border="1px solid red";
        lnamevalidcheck.style.color="red";
        lnamevalidcheck.style.visibility="visible";
        // return false;
    } 
    else{
        lnamevalidcheck.innerHTML="Valid";
        lnamevalidcheck.style.color="green";
        lnamevalidcheck.style.visibility="visible";
        
    }

    //email

    if(useremail.value.trim()==""){
        useremailcheck.innerHTML="Email is required";
        useremail.style.border="1px solid red";
        useremailcheck.style.color="red";
        useremailcheck.style.visibility="visible";
        // return false;
    }
    else if(!emailRegx.test(useremail.value)){
        useremailcheck.innerHTML="Invalid format";
        useremail.style.border="1px solid red";
        useremailcheck.style.color="red";
        useremailcheck.style.visibility="visible";
        // return false;
    }
    else{
        useremailcheck.innerHTML="Valid";
        useremailcheck.style.color="green";
        useremailcheck.style.visibility="visible";
        
    }

    // Date of birth

    if(dateOfBirth.value.trim()==""){
        dateOfBirthcheck.innerHTML="Date is required";
        dateOfBirth.style.border="1px solid red";
        dateOfBirthcheck.style.color="red";
        dateOfBirthcheck.style.visibility="visible";
        // return false;
    }
    else if(!dateRegx.test(dateOfBirth.value)){
        dateOfBirthcheck.innerHTML="Invalid format";
        dateOfBirth.style.border="1px solid red";
        dateOfBirthcheck.style.color="red";
        dateOfBirthcheck.style.visibility="visible";
        // return false;
    }
    else{
        dateOfBirthcheck.innerHTML="Valid";
        dateOfBirthcheck.style.color="green";
        dateOfBirthcheck.style.visibility="visible";
    }
    // mobile number
    if(mobileNumber.value.trim()==""){
        mobileNumberValidCheck.innerHTML="Mobile number is required";
        mobileNumber.style.border="1px solid red";
        mobileNumberValidCheck.style.color="red";
        mobileNumberValidCheck.style.visibility="visible";
        // return false;
    }
    else if(!mobileRegx.test(mobileNumber.value)){
        mobileNumberValidCheck.innerHTML="Invalid";
        mobileNumber.style.border="1px solid red";
        mobileNumberValidCheck.style.color="red";
        mobileNumberValidCheck.style.visibility="visible";
        // return false;
    }
    else{
        mobileNumberValidCheck.innerHTML="Valid";
        mobileNumberValidCheck.style.color="green";
        mobileNumberValidCheck.style.visibility="visible";
    }
    //Address

    if(addressLine1.value.trim()==""){
        addressvalidcheck.innerHTML="Address is required";
        addressLine1.style.border="1px solid red";
        addressvalidcheck.style.color="red";
        addressvalidcheck.style.visibility="visible";
        // return false;
    }
    else if(!addressRegx.test(addressLine1.value)){
        addressvalidcheck.innerHTML="Invalid";
        addressLine1.style.border="1px solid red";
        addressvalidcheck.style.color="red";
        addressvalidcheck.style.visibility="visible";
        // return false;
    }
    else{
        addressvalidcheck.innerHTML="Valid";
        addressvalidcheck.style.color="green";
        addressvalidcheck.style.visibility="visible";
    }

    //city
    if(city.value.trim()==""){
        cityvalidcheck.innerHTML="City is required";
        city.style.border="1px solid red";
        cityvalidcheck.style.color="red";
        cityvalidcheck.style.visibility="visible";
        // return false;
    }
    else if(!cityRegx.test(city.value)){
        cityvalidcheck.innerHTML="Invalid";
        city.style.border="1px solid red";
        cityvalidcheck.style.color="red";
        cityvalidcheck.style.visibility="visible";
        // return false;
    }
    else{
        cityvalidcheck.innerHTML="Valid";
        cityvalidcheck.style.color="green";
        cityvalidcheck.style.visibility="visible";
    }
    //Pincode
    if(pincode.value.trim()==""){
        pincodevalidcheck.innerHTML="Pincode is required";
        pincode.style.border="1px solid red";
        pincodevalidcheck.style.color="red";
        pincodevalidcheck.style.visibility="visible";
        // return false;
    }
    else if(!zipRegx.test(pincode.value)){
        pincodevalidcheck.innerHTML="Invalid";
        pincode.style.border="1px solid red";
        pincodevalidcheck.style.color="red";
        pincodevalidcheck.style.visibility="visible";
        // return false;
    }
    else{
        pincodevalidcheck.innerHTML="Valid";
        pincodevalidcheck.style.color="green";
        pincodevalidcheck.style.visibility="visible";
    }



    
   
    


}
var permanentAddress1=document.getElementById("permanentaddressLine1");
var permanentAddress2=document.getElementById("permanentaddressLine2");
var permanentCity=document.getElementById("permanentcity");
var permanentPincode=document.getElementById("permanentpincode");
function CopyAddress(){
   

    permanentAddress1.value=addressLine1.value;
    permanentAddress2.value=addressLine2.value;
    permanentCity.value=city.value;
    permanentPincode.value=pincode.value;

}
